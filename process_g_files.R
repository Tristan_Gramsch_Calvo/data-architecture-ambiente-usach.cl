#####
# This is what I do.
# You can use this function to remove all objects except one rm(list = dplyr::setdiff(ls(), "x"))
# Setup. 
#####
library(tidyverse)
library(rvest)

#####
# Summarize variables for one table that starts with G. 
#####
# Read and clean the data
example_table <- read.table("http://ambiente.usach.cl/globo/G8-120618.dat", header = TRUE)

example_table <- tibble::rownames_to_column(example_table, "Fecha")
# Move name columns to the left 
names(example_table)[1:(ncol(example_table)-1)] <- names(example_table)[2:ncol(example_table)]

example_table <- example_table %>% 
  rename(Fecha = 6)

example_table %>%
  summarize_at(vars(Temp_globo:Rad_solar), mean, na.rm = TRUE)
